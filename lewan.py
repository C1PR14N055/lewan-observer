import requests
import os
import time

url = ""
token = ""

def takePic():
    os.system("raspistill -vf -w 1296 -h 972 -q 70 -o picture.jpg")
    with open("picture.jpg", "rb") as f:
        r = requests.post(url, data={ "token": token }, files={ "picture": f }, timeout=5)
        print(r.text)

for i in range(4):
    try:
        takePic()
        time.sleep(10)
    except:
        pass