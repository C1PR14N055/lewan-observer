<?php 

define("MAX_UPLOAD_SIZE", 10 * 1000 * 1000); // X * KB * B = MB
define("UPLOADS_DIR", "uploads/"); 
define("SECRET_TOKEN", "o*JJ_3iiyJ[yM");

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if ($_POST["token"] == SECRET_TOKEN) {
        if (is_uploaded_file($_FILES["picture"]["tmp_name"])) {
            $uploadStatus = upload_image($_FILES["picture"]);
            if ($uploadStatus[0]) {
                send_response(1, "Upload ok!");
            } else {
                send_response(0, "Fisierul nr. " . $i . " nu s-a putut incarca: " . $upload[1]);
            }
        }
    } else { 
        send_response(0, "Access denied! - " . $_POST["token"]);
    }
} 

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    $data = array_slice(scandir(UPLOADS_DIR), 2);
    echo json_encode($data);
}

function upload_image($file) {
    if (is_uploaded_file($file["tmp_name"])) {
        $fileType =  strtolower(pathinfo(basename($file["name"]), PATHINFO_EXTENSION));
        // Allow only certain file formats
        $file_checks_out = false;
        if($fileType == "jpeg" || $fileType == "jpg") {
            //$mime = mime_content_type($file["tmp_name"]);
            //if(!strstr($mime, "image/")){
            //    return array(0, "Fisierul incarcat nu e imagine!");
            //}
            $file_checks_out = true;
        }
        if (!$file_checks_out) {
            return array(0, "Fisierul incarcat nu e imagine!");
        }
        $randomString = date("YmdHis"); // datetime OR generate_random_string(18);
        $fileName = $randomString . "." . $fileType;
        $target_file = UPLOADS_DIR . $fileName;
        // Check if file already exists
        if (file_exists($target_file)) {
            return array(0, "Ne pare rau, acest fisier deja exista!");
        }
        // Check file size
        if ($file["size"] > MAX_UPLOAD_SIZE) {
            return array(0, "Fisierul incarcat e prea mare!");
        }
        if (move_uploaded_file($file["tmp_name"], $target_file)) {
            chmod($target_file, 0755);
            try {
            //    exec("convert -auto-gamma -auto-level -normalize '$target_file' '$target_file'");
            }
            catch (Exception $e) {
                
            }
            return array(1, "Fisierul a fost incarcat", $fileName);
        } else {
            return array(0, "Ne pare rau, fisierul nu s-a putut incarca!");
        }
    } else {
        return array(0, "Incarca cel putin o poza!");
    }
}

function generate_random_string($length = 6) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function send_response($status, $message) {
    die("{\"status\": \"" . $status . "\", \"message\": \"" . $message . "\"}");   
}

